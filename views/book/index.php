<?php require 'views/header.php'; ?>
<main>

        <h1>Lista de libros</h1>

        <table>
            <tr>
                <th>id</th>
                <th>Título</th>
                <th>Autor</th>
                <th>Páginas</th>
                <th>Categoría</th>
                <th>Acciones</th>
            </tr>
            <?php foreach ($books as $book): ?>
            <tr>
                <td><?php echo $book->id ?></td>
                <td><?php echo $book->title ?></td>
                <td><?php echo $book->author ?></td>
                <td><?php echo $book->pages ?></td>
                <td><?php echo $book->cathegory ? $book->cathegory->name : '' ?></td>

                <td>
                    <a href="/book/edit/<?php echo $book->id ?>">Editar</a>
                    <a href="/book/delete/<?php echo $book->id ?>">Borrar</a>
                    <a href="/book/remember/<?php echo $book->id ?>">Recordar</a>
                </td>
            </tr>
            <?php endforeach ?>
        </table>


        <p>
            <a href="/book/create">nuevo</a>
        </p>



</main>
<?php require 'views/footer.php'; ?>
