

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;


INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'client', NULL, NULL);



DROP TABLE `users`;
CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL DEFAULT '2',
  `surname` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `role_id`, `surname`) VALUES
(1, 'Pepe', 'pepe@gmail.com', '$2y$10$Wv8aZwvImD3a94Y.d4sBUe/.b3leae2brrdfKvV4JE1jkdxV6qFYa', NULL, NULL, NULL, 1, 'García López'),
(2, 'Juan', 'juan@gmail.com', '$2y$10$GA8gjCpJafuHRIRmlmEbHeBbHmzExNRyFOZtLHSXilRGul62wMrE.', 'wlTRyTzY6u6HJ5DesgtAJgx4scyyCYYtPCztKBR4WSGQJt9FeK3MY17iKXcR', NULL, '2017-03-03 05:34:44', 2, 'Sánchez Moreno'),
(3, 'Ana', 'ana@gmail.com', '$2y$10$84HWrIKUxOE5H41uObdNw.N/g1z.sEgFCp2fn4L9oy2DFcHx7Msh6', NULL, NULL, NULL, 2, 'Merino Higuera'),
(4, 'Yolanda', 'yolanda@gmail.com', '$2y$10$gElO4seR31xYDpdrD7pHX.bqvnFO8j1UdglqA0Iw8lvBtW.AGqB2q', NULL, NULL, NULL, 2, 'Rodrigo Sorribas');


ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);


ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

